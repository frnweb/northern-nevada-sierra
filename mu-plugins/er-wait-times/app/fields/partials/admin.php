<?php
use StoutLogic\AcfBuilder\FieldsBuilder;

$optionsER = new StoutLogic\AcfBuilder\FieldsBuilder('er_options');

$optionsER
    ->setLocation('options_page', '==', 'er-wait-settings');

$optionsER
->addGroup('facility_list')
    // List
    ->addRepeater('facility_repeater', [
        'label' => 'List Items',
        'button_label' => 'Add List item',
        'layout' => 'block',
        ])
        ->addText('facility_title', [
            'label' => 'Facility Name'
        ])

        ->addText('facility_code', [
            'label' => 'ER Facility Code'
        ])
        ->setInstructions('Special Code provided by UHS')

        ->addText('facility_url', [
            'label' => 'Facility URL'
        ])
        ->setInstructions('Website URL')
    ->endRepeater()
->endGroup();

return $optionsER;