<?php
// MAP
        function sl_map() {
            $phone = get_field('phone', 'option');
            $map = get_field('address', 'option');
            $google_map = $map['google_map'];
    
            return '<div class="sl_module sl_module--location sl_location sl_location--shortcode">
                <div class="sl_row">
                    <div class="sl_location__content sl_cell large-5 medium-6">
                        <div class="sl_location__card sl_card sl_card--location">
            
                            <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <span itemprop="name"><strong>'. $map['location_name'] .'</strong></span><br>
                                <span itemprop="streetAddress">'. $map['street_address'] .'</span><br>
                                <span itemprop="addressLocality">'. $map['city'] .', '. $map['state'] .'</span> 
                                <span itemprop="postalCode">'. $map['zip_code'] .'</span><br>
                                <a href="tel: ' . $phone['main'] . '" style="white-space:nowrap;"> '. $phone['main'] .' </a>
                            </p>
            
                            <a href="https://www.google.com/maps/dir/?api=1&destination='. $google_map['lat'] .','. $google_map['lng'] .'" target="_blank" rel="noreferrer"><i class="fal fa-map"></i> Get Directions</a>
                        </div>
                    </div>
                     <div class="sl_location__map sl_cell large-7 medium-6">
                        <div class="sl_module__map">
                            <div class="marker" data-lat="'. $google_map['lat'] .'" data-lng="'. $google_map['lng'] .'">
                                <div class="sl_card sl_card--map">
                                    <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                        <span itemprop="name"><strong>'. $map['location_name'] .'</strong></span><br>
                                        <span itemprop="streetAddress">'. $map['street_address'] .'</span><br>
                                        <span itemprop="addressLocality">'. $map['city'] .', '. $map['state'] .'</span> 
                                        <span itemprop="postalCode">'. $map['zip_code'] .'</span><br>
                                    </p>
                                    <a class="sl_button sl_button--simple" href="https://www.google.com/maps/dir/?api=1&destination='. $google_map['lat'] .','. $google_map['lng'] .'" target="_blank" rel="noreferrer">Directions</a>
                                </div>
                            </div>
                        </div>
                     </div>
                 </div>
            </div>';
        }
        add_shortcode( 'map', 'sl_map' );
//MAP
?>