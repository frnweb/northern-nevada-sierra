<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/admin
 * @author     eSkyCity <support@eskycity.com>
 */
class Eskycity_Sanity_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eskycity_Sanity_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eskycity_Sanity_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/eskycity-sanity-admin.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eskycity_Sanity_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eskycity_Sanity_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/eskycity-sanity-admin.js', array('jquery'), $this->version, false);
		
		wp_localize_script($this->plugin_name, 'admin_vars', array('admin_nonce' => wp_create_nonce('admin-nonce')));
		
		wp_enqueue_script($this->plugin_name . '-repeater-js', plugin_dir_url(__FILE__) . 'js/eskycity-sanity-repeater.js', array('jquery', 'wp-util'), $this->version, true);
	}

	public function add_admin_menu() {
		
		add_options_page('eSkyCity Sanity Options', 'eSkyCity Sanity', 'manage_options', 'eskycity-sanity', array($this, 'admin_page_display'));
		
	}

	public function admin_page_display() {

		include 'partials/eskycity-sanity-admin-display.php';
	}

	public function admin_init() {

		$this->add_settings_section();

		$this->add_settings_fields();
		
		$this->update_settings();
		
	}

	public function add_settings_section() {

		add_settings_section('eskycity-santiy-general-section', 'API Connectivity',
			function () {
				echo "<p>Sanity.io API Connectivity Settings</p>";
			}, 'eskycity-santiy-settings-page' );
		
		add_settings_section('eskycity-santiy-urls-section', 'URL Rewrites',
			function () {
				echo "<p>Paging and URL rewrite configuration</p>";
			}, 'eskycity-santiy-urls-page' );
			
		add_settings_section('eskycity-santiy-output-section', 'Output',
			function () {
				echo "<p>Output and Display Settings</p>";
			}, 'eskycity-santiy-output-page' );
		
	}

	public function add_settings_fields() {

		add_settings_field('eskycity_sanity_projectid', 'Project ID', 
				array( $this, 'text_field_callback' ),
				'eskycity-santiy-settings-page', 'eskycity-santiy-general-section', 
				array( 'name' => 'eskycity_sanity_projectid', 'value' => get_option('eskycity_sanity_projectid') ));
		
		add_settings_field('eskycity_sanity_dataset', 'Dataset', 
				array( $this, 'text_field_callback' ),
				'eskycity-santiy-settings-page', 'eskycity-santiy-general-section', 
				array( 'name' => 'eskycity_sanity_dataset', 'value' => get_option('eskycity_sanity_dataset') ));
		
		add_settings_field('eskycity_sanity_cdn', 'Enable CDN', 
				array( $this, 'checkbox_field_callback' ),
				'eskycity-santiy-settings-page', 'eskycity-santiy-general-section',
				array( 'name' => 'eskycity_sanity_cdn', 'value' => get_option('eskycity_sanity_cdn') ));
		
		add_settings_field('eskycity_sanity_apiversion', 'API Version', 
				array( $this, 'text_field_callback' ),
				'eskycity-santiy-settings-page', 'eskycity-santiy-general-section',
				array( 'name' => 'eskycity_sanity_apiversion', 'value' => get_option('eskycity_sanity_apiversion') ));
		
//		add_settings_field('eskycity_sanity_urls_0', 'Rewrite Rule 0', 
//				array( $this, 'textrepeaterpair_field_callback' ),
//				'eskycity-santiy-urls-page', 'eskycity-santiy-urls-section', 
//				array( 'key' => 'eskycity_sanity_url_key_0', 'key_value' => get_option('eskycity_sanity_urls' )['eskycity_sanity_url_key_0'], 
//						'data' => 'eskycity_sanity_url_data_0', 'data_value' => get_option('eskycity_sanity_urls' )['eskycity_sanity_url_data_0'] ) );
		
		add_settings_field('eskycity_sanity_urls', 'Rule List', 
				array( $this, 'textrepeaterpair_field_callback' ),
				'eskycity-santiy-urls-page', 'eskycity-santiy-urls-section' );
		
		add_settings_field('eskycity_sanity_pagesize', 'Default Page Size', 
				array( $this, 'number_field_callback' ),
				'eskycity-santiy-output-page', 'eskycity-santiy-output-section',
				array( 'name' => 'eskycity_sanity_pagesize', 'value' => get_option('eskycity_sanity_pagesize') ));
		
		add_settings_field('eskycity_sanity_search_autocomplete', 'Enable Search Autocomplete', 
				array( $this, 'checkbox_field_callback' ),
				'eskycity-santiy-output-page', 'eskycity-santiy-output-section',
				array( 'name' => 'eskycity_sanity_search_autocomplete', 'value' => get_option('eskycity_sanity_search_autocomplete') ));
	}
	
	public function text_field_callback( $args ) {
		
		if ( is_array($args) ) {
			
			$field_name = ( isset( $args['name'] ) ? esc_html( $args['name'] ) : '' );
			$field_value = ( isset( $args['value'] ) ? esc_html( $args['value'] ) : '' );
			
			$field_html = '<input type="text" name="' . $field_name . '" value="' . $field_value . '" />';
			
			echo $field_html;
			
		}
		
	}
	
	public function textrepeaterpair_field_callback( $args ) {
		
		$url_settings = get_option( 'eskycity_sanity_urls' );
		
		if ( is_array( $url_settings ) && ! empty( $url_settings ) ) {
			
			$field_html = "";
			
			foreach ( $url_settings as $key => $value ) {
				
				$key = strtolower( esc_attr ( $key ) );
				$value = strtolower( esc_attr ( $value ) );
				
				if ( ! empty( $value ) ) {
					
					if ( substr( $key, 0, 5 ) == 'slug_' ) {
					
						$field_id = intval( explode( '_', $key )[1] );
					
						$field_html .= '<tr><th scope="row">&nbsp;Rewrite Rule ' . $field_id . '</th><td></td></tr>';
						
						$field_html .= '<tr><td>Page Slug</td><td><input type="text" name="eskycity_sanity_urls[' .$key . ']" value="' . $value . '" /></td></tr>';
					
					}
					elseif ( substr( $key, 0, 9 ) == 'queryvar_' ) {

						$field_html .= '<tr><td>Query Variable</td><td><input type="text" name="eskycity_sanity_urls[' .$key . ']" value="' . $value . '" /></td></tr>';

					}
					
				}
				
			}
			
			echo $field_html;
			
		}
		
	}
	
	public function number_field_callback( $args ) {
		
		if ( is_array($args) ) {
			
			$field_name = ( isset( $args['name'] ) ? esc_html( $args['name'] ) : '' );
			$field_value = ( isset( $args['value'] ) ? intval( $args['value'] ) : '' );
			
			if ( $field_value == 0 ) {
				
				$field_value = 25;
				
			}
			
			$field_html = '<input type="number" name="' . $field_name . '" value="' . $field_value . '" style="width:55px" />';
			
			echo $field_html;
			
		}
		
	}
	
	public function checkbox_field_callback( $args ) {
		
		if ( is_array($args) ) {
			
			$field_name = ( isset( $args['name'] ) ? esc_html( $args['name'] ) : '' );
			
			$field_html = '<input type="checkbox" name="' . $field_name . '" value="1" ' . checked( 1, get_option($field_name), false ) . ' />';
			
			echo $field_html;
			
		}
		
	}
	
	public function update_settings() {
		
		$text_sanitizer = array( 'sanitize_callback' => 'sanitize_text_field' );
		$int_sanitizer = array( 'sanitize_callback' => 'absint' );
		
		register_setting( 'eskycity-sanity-settings-page-options-group', 'eskycity_sanity_projectid', $text_sanitizer );
		register_setting( 'eskycity-sanity-settings-page-options-group', 'eskycity_sanity_dataset', $text_sanitizer );
		register_setting( 'eskycity-sanity-settings-page-options-group', 'eskycity_sanity_cdn', $int_sanitizer );
		register_setting( 'eskycity-sanity-settings-page-options-group', 'eskycity_sanity_apiversion', $text_sanitizer );
		
		register_setting( 'eskycity-sanity-settings-page-urls-group', 'eskycity_sanity_urls' );
		
		register_setting( 'eskycity-sanity-settings-page-output-group', 'eskycity_sanity_pagesize', $int_sanitizer );
		register_setting( 'eskycity-sanity-settings-page-output-group', 'eskycity_sanity_search_autocomplete', $int_sanitizer );
	
	}
	
	public function update_rewrites() {
			
		$url_settings = get_option( 'eskycity_sanity_urls' );
		
		// Check if URL settings exist
		if ( is_array( $url_settings ) && ! empty( $url_settings ) ) {
		
			// Loop through settings
			foreach ( $url_settings as $key => $value ) {
				
				$key = strtolower( esc_attr ( $key ) );
				$value = strtolower( esc_attr ( $value ) );
				
				// Slug found with value
				if ( ( substr( $key, 0, 5 ) == 'slug_' ) && ! empty( $value ) ) {
					
					// Get matching queryvar id
					$field_id = intval( explode( '_', $key )[1] );
					$queryvar = 'queryvar_' . $field_id;
					
					// Check for matching queryvar with value
					if ( array_key_exists( $queryvar, $url_settings ) ) {
						
						// value exists, set redirect rule
						add_rewrite_rule( $value .'/?([^/]*)', 'index.php?pagename=' . $value . '&' . $url_settings[$queryvar] . '=$matches[1]', 'top' );
						
					}
				}
			}
			
			// Flush WP rewrite rules to ensure rewrites work immediately when updated here
			flush_rewrite_rules();
		}
	}
	
	public function add_plugin_action_links( $links ) {
		
		$links[] =  '<a href="' . admin_url( 'options-general.php?page=eskycity-sanity' ) . '">Settings</a>' ;
		return  $links;
		
	}
	
	public function sanity_check() {

		if ( ! isset( $_POST['admin_nonce'] ) || ! wp_verify_nonce( $_POST['admin_nonce'], 'admin-nonce' ) ) {
			
			die( 'Nonce failed' );
			
		}
		
		$connect_message = "Connection Failed";

		try {
			$sanity_connector = new Eskycity_Sanity_Connector();
			$sanity_connector->sanity_connect();
			$sanity_query = new Eskycity_Sanity_Query( $sanity_connector );
			$sanity_query->fetch_custom( 'count(*)' );
			$result = $sanity_query->get_result();
			
			if ( is_int( $result ) ) {
				$connect_message = "Connection Successful! Found " . $result . " objects.";
			}
		}
		catch (Exception $ex) {
			$connect_message .= ": " . $ex->getMessage();
		}
		
		if ( $connect_message == "Connection Failed" && ! strtotime( $sanity_connector->get_api_version() ) ) {
			
			// There's no additional error information, but the API version appears to be in the wrong format (not a valid date)
			$connect_message .= ": Check API Version";
			
		}
		
		echo $connect_message;

		wp_die(); // this is required to terminate immediately and return a proper response
	}

}
