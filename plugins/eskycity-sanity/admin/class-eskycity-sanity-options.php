<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/admin
 * @author     eSkyCity <support@eskycity.com>
 */
class Eskycity_Sanity_Options {

	public static function get_options_tab() {
		
		$result = null;
		
		if ( isset( $_GET['tab'] ) ) {
			
			$result = strtolower( trim( sanitize_text_field( $_GET['tab'] ) ) );
			
		}
		
		return $result;
		
	}
	
	public static function get_options_class( $tab = NULL ) {
		
		$result = "nav-tab";
		
		if ( $tab == self::get_options_tab() ) {
			
			$result .= " nav-tab-active";
			
		}
		
		return $result;
		
	}

}
