(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

})( jQuery );

jQuery(document).ready(function($) {
    $('#sanity-check').submit(function() {
        
        $('#sanity-check-loading').show();
        $('#sanity-check-button').prop('disabled', true);
        
        var data = { action: 'sanity_check', admin_nonce: admin_vars.admin_nonce };
        
        //alert(admin_nonce);
        
        jQuery.post(ajaxurl, data, function(response) {

            var status = 'error';
            
            if (response.toLowerCase().includes('success')){
                status = 'success';
            }
            
            $('#sanity-check-result').html('<div class="notice notice-' + status + ' is-dismissible"><p>' + response + '</p></div>');
        });
        
        $('#sanity-check-loading').fadeOut('slow');
        $('#sanity-check-button').prop('disabled', false);
        
       return false;
    });
});

function test() {
    document.getElementById('url-repeater-fields').innerHTML += "<input type='text' name='test' />";
}

//jQuery(document).ready(function($) {
//    $('#url-repeater').submit(function() {
//        
//        alert('test');
//        
//        return false;
//        
//    });    
//});