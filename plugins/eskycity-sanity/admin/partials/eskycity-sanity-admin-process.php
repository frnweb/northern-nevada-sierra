<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/admin/partials
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

if ( isset( $_POST ) && ! empty( $_POST ) )
{
	
	//var_dump($_POST);
	
	$use_cdn = 0;
	
	if ( isset( $_POST['eskycity_sanity_cdn'] ) ) {
		
		$use_cdn = filter_var( $_POST['eskycity_sanity_cdn'], FILTER_SANITIZE_NUMBER_INT );
		
	}
	
	update_option( 'eskycity_sanity_projectid', sanitize_text_field( $_POST['eskycity_sanity_projectid'] ) );
	update_option( 'eskycity_sanity_dataset', sanitize_text_field( $_POST['eskycity_sanity_dataset'] ) );
	update_option( 'eskycity_sanity_cdn', $use_cdn );
	update_option( 'eskycity_sanity_apiversion', sanitize_text_field($_POST['eskycity_sanity_apiversion'] ) );
	
}

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
	<h1>eSkyCity Sanity Settings</h1>

	<form method="post" action="">
		<table class="form-table">
			<tbody>
				<tr>
					<th scope="row">
						<label for="eskycity_sanity_projectid">Project ID</label>
					</th>
					<td>
						<input name="eskycity_sanity_projectid" type="text" id="eskycity_sanity_projectid" class="regular-text" 
							   value="<?php echo get_option('eskycity_sanity_projectid'); ?>">
						<p class="description" id="tagline-description"></p>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="eskycity_sanity_dataset">Dataset</label>
					</th>
					<td>
						<input name="eskycity_sanity_dataset" type="text" id="eskycity_sanity_dataset" class="regular-text"
							   value="<?php echo get_option('eskycity_sanity_dataset'); ?>">
					</td>
				</tr>
				<tr>
					<th scope="row">Use CDN?</th>
					<td>
						<label for="eskycity_sanity_cdn">
							<input name="eskycity_sanity_cdn" type="checkbox" id="eskycity_sanity_cdn" value="1" 
								<?php checked( 1, get_option('eskycity_sanity_cdn'), true ); ?>>
						</label>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="eskycity_sanity_apiversion">API Version</label>
					</th>
					<td>
						<input name="eskycity_sanity_apiversion" type="text" id="eskycity_sanity_apiversion" class="regular-text"
							   value="<?php echo get_option('eskycity_sanity_apiversion'); ?>" >
					</td>
				</tr>
	
			</tbody>
		</table>

		<?php submit_button(); ?>
	</form>
</div>