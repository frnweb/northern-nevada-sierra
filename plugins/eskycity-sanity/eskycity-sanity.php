<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.eskycity.com
 * @since             1.0.0
 * @package           Eskycity_Sanity
 *
 * @wordpress-plugin
 * Plugin Name:       eSkyCity Sanity
 * Plugin URI:        https://www.eskycity.com
 * Description:       Sanity API Integration
 * Version:           1.0.0
 * Author:            eSkyCity
 * Author URI:        https://www.eskycity.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       eskycity-sanity
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'ESKYCITY_SANITY_VERSION', '1.0.0' );

define( 'ESKYCITY_SANITY_BASE_FILE', __FILE__ );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-eskycity-sanity-activator.php
 */
function activate_eskycity_sanity() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-eskycity-sanity-activator.php';
	Eskycity_Sanity_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-eskycity-sanity-deactivator.php
 */
function deactivate_eskycity_sanity() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-eskycity-sanity-deactivator.php';
	Eskycity_Sanity_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_eskycity_sanity' );
register_deactivation_hook( __FILE__, 'deactivate_eskycity_sanity' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-eskycity-sanity.php';



/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_eskycity_sanity() {

	$plugin = new Eskycity_Sanity();
	$plugin->run();
}
run_eskycity_sanity();