<?php

/**
 * Sanity API connectivity
 *
 * This class is used to connect to the Sanity API and provide access to the SanityClient
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/includes
 * @author     eSkyCity <support@eskycity.com>
 */
require_once plugin_dir_path( __FILE__ ) . '../vendor/autoload.php';

use Sanity\Client as SanityClient;

class Eskycity_Sanity_Connector {

	private $project_id;
	private $dataset;
	private $use_cdn;
	private $api_version;
	
	public $sanity_client;
	
	public function __construct( ) {
				
		$this->project_id = sanitize_text_field( get_option( 'eskycity_sanity_projectid' ) );
		$this->dataset = sanitize_text_field( get_option( 'eskycity_sanity_dataset' ) );
		$this->use_cdn = boolval( filter_var( get_option( 'eskycity_sanity_cdn' ), FILTER_SANITIZE_NUMBER_INT ) );
		$this->api_version = sanitize_text_field( get_option( 'eskycity_sanity_apiversion' ) );	
	}
	
	public function change_connection( string $project_id, string $dataset, bool $use_cdn, string $api_version )
	{
		$this->project_id = $project_id;
		$this->dataset = $dataset;
		$this->use_cdn = $use_cdn;
		$this->api_version = $api_version;	
	}
	
	public function sanity_connect() {
		
		if ( ! empty( $this->project_id ) && ! empty( $this->dataset ) && isset ( $this->use_cdn ) && ! empty( $this->api_version ) )
		{
			
			try {

				$this->sanity_client = new SanityClient([
				'projectId' => $this->project_id,
				'dataset' => $this->dataset, 
				'useCdn' => $this->use_cdn, 
				'apiVersion' => $this->api_version
			  ]);

			}
			catch ( Exception $ex ) {

				error_log("eSkyCity Sanity Error: " . $ex->getMessage());

			}
			
		}
		
	}
	
	public function get_project_id() {
		
		return $this->project_id;
		
	}
	
	public function get_dateset() {
		
		return $this->dataset;
		
	}
	
	public function get_cdn() {
		
		return $this->cdn;
		
	}
	
	public function get_api_version() {
		
		return $this->api_version;
		
	}
}
