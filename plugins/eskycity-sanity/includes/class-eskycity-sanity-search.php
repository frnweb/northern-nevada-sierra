<?php

/**
 * Sanity Search String Builder
 *
 * This class is used generate GROQ queries from simple public methods, and provide access to the resulting query and JSON or scalar result.
 *
 * Also facilitates custom GROQ queries.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/includes
 * @author     eSkyCity <support@eskycity.com>
 */
class Eskycity_Sanity_Search {

	private $result_search_string;

	public function __construct() {

		$this->result_search_string = "";
		
	}
	
	public function get_search_string() {
		
		return $this->result_search_string;
		
	}
	
	public function is_search( array $search_variables ) {
		
		$result = false;
		
		if ( ! empty( $search_variables ) ) {
			
			foreach ( $search_variables as $search_variable ) {
				
				if ( isset ( $_GET[$search_variable] ) ) {
					
					$result = true;
					
				}
				
			}
			
		}
			
		return $result;
		
	}
	
	public function add_search_string( array $search_fields, array $search_values, 
			bool $split_values = true, bool $wildcard = true, string $field_comparison_operator = NULL, 
			string $string_comparison_operator = NULL, string $query_comparison_operator = NULL ) {
		
		$result = "";
		
		if ( empty( $search_fields ) || empty( $search_values ) ) {
			
			return $result;
			
		}
		
		$wildcard_output = "";
		
		if ( is_null( $field_comparison_operator ) ) {
			
			$field_comparison_operator = "||";
			
		}
		
		if ( is_null( $string_comparison_operator ) ) {
			
			$string_comparison_operator = "&&";
			
		}
		
		if ( is_null( $query_comparison_operator ) ) {
			
			$query_comparison_operator = "&&";
			
		}
		
		if ( $wildcard ) {
			
			$wildcard_output = "*";
			
		}
		
		foreach ( $search_values as $search_value ) {

			if ( empty( trim( $search_value ) ) ) {
				
				// Skip cases where the search value is empty
				continue;
				
			}
			
			// Search value contains a space and should be split to search each string individually
			if ( $split_values && ( strpos( $search_value, ' ' ) !== false ) ) {
				
				$search_value = explode( ' ', $search_value );

				foreach ( $search_value as $search_value_part ) {
					
					$search_value_part = str_replace( "-", " ", $search_value_part);
					
					$result .= "(";
					
					foreach ( $search_fields as $search_field ) {
						
						$result .= $search_field . ' match "' . $wildcard_output . sanitize_text_field( $search_value_part ) . $wildcard_output . '" ' . $field_comparison_operator . ' ';
						
					}

					// Remove last field comparison operator
					$result = rtrim( $result, $field_comparison_operator . ' ' );
					
					$result .= ") " . $string_comparison_operator . ' ';
				}
				
				$result = rtrim( $result, $string_comparison_operator . ' ' );
			}
			else {
				
				$result .= "(";
				
				foreach ( $search_fields as $search_field ) {
				
					$search_value = str_replace( "-", " ", $search_value );
					
					// Searching for a string, not an array of strings
					$result .= sanitize_text_field( $search_field ) . ' match "' . 
							$wildcard_output . sanitize_text_field( $search_value ) . 
							$wildcard_output . '" ' . $field_comparison_operator . ' ';

				}
				
				// Remove last field comparison operator
				$result = rtrim( $result, $field_comparison_operator . ' ' );
				
				$result .= ") " . $string_comparison_operator . ' ';
				
			}
			
			// Remove last string comparison operator
			$result = rtrim( $result, $string_comparison_operator . ' ' );
			
		}
		
		// Add query comparision operator, only if this is not the first query. Otherwise, no other query to compare.
		if ( ! empty ( $this->result_search_string ) ) {
			$result = ' ' . $query_comparison_operator . ' ' . $result;
		}
		
		// Remove last query comparison operator
		$result = rtrim( $result, $query_comparison_operator . ' ' );
		
		$this->result_search_string .= $result;
		
	}
	
}
