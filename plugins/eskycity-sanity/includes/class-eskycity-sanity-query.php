<?php

/**
 * Sanity Query Builder
 *
 * This class is used generate GROQ queries from simple public methods, and provide access to the resulting query and JSON or scalar result.
 *
 * Also facilitates custom GROQ queries.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/includes
 * @author     eSkyCity <support@eskycity.com>
 */
class Eskycity_Sanity_Query {

	private $connector;
	private $client;
	
	private $_document_type;
	private $_search_array;
	
	private $result_query;
	private $result;

	public function __construct( Eskycity_Sanity_Connector $sanity_connector ) {

		$this->connector = $sanity_connector;

		$this->client = $this->connector->sanity_client;
	}

	public function fetch( string $document_type, array $field_array = NULL, array $search_array = NULL,
			int $offset = NULL, int $count = NULL, array $orderby_array = NULL ) {
		
		$this->_document_type = $document_type;
		$this->_search_array = $search_array;
		
		if ( isset( $this->client )) {

			$query = "*[_type == \"" . $document_type . "\"";

			if ( ! is_null( $search_array ) ) {

				//var_dump($search_array);
				
				foreach ( $search_array as $search ) {
					
//					if ( strpos( $search, "||" ) ) {
//						
//						$query .= $search;
//						
//					}
//					else {
						$query .= " && ". $search;
//					}
				}

			}

			$query .= "]";

			// Add fields
			if ( ! is_null( $field_array ) ) {

				$query .= " { " . implode( ", ", $field_array ) . " }";
			}

			// Add pipe to prepare for remaining variables
			if ( ! is_null( $offset ) || ! is_null( $count ) || ! is_null( $orderby_array ) ) {

				$query .= " | ";

			}

			// Add order
			if ( ! is_null( $orderby_array ) ) {

				$order_query = "order (";

				foreach ( $orderby_array as $orderby_field => $orderby_direction ) {
					$order_query .= $orderby_field . " " . $orderby_direction . ", ";
				}

				$order_query = rtrim( $order_query, ", " ) . ")";

				$query .= $order_query;

			}

			// Add slice
			if ( ! is_null( $offset ) || ! is_null( $count ) ) {

				$slice_query = "[";

				if ( ! is_null( $offset ) && ! is_null( $count ) ) {

					$last_record = $offset + $count;

					if ( $count < 1 ) {

						// Can't return less than 1 record, so default to 25 records
						$last_record = $offset + 25;

					}

					$slice_query .= strval( $offset ) . "..." . strval( $last_record );

				}
				else if ( ! is_null( $offset ) && is_null( $count ) ) {

					$slice_query .= strval( $offset ) . "..." . strval( $offset + 25 );

				}
				else if ( is_null( $offset ) && ! is_null( $count ) ) {

					$last_record = $count;

					if ( $count < 1 ) {
						// Can't return less than 1 record, so default to 25 records
						$last_record = 25;

					}

					$slice_query .= "0..." . strval( $last_record );

				}

				$slice_query .= "]";

				$query .= $slice_query;
			}


			$this->result_query = $query;

			$query_result = $this->client->fetch ( $query );

			$this->result = $query_result;
			
		}
		
	}

	public function fetch_count( string $document_type, array $search_array = NULL ) {

		$result_count = 0;
		
		if ( isset( $this->client )) {
			
			$query = "count(*[_type == \"" . $document_type . "\"";

			if ( ! is_null( $search_array ) ) {

				foreach ( $search_array as $search ) {
					$query .= " && ". $search;
				}

			}

			$query .= "])";

			$this->result_query = $query;

			$count = $this->client->fetch ( $query );

			$result_count = $count;
			
		}

		return $result_count;
		
	}

	public function fetch_custom( string $custom_query ) {

		if ( isset( $this->client )) {
			
			$query_result = $this->client->fetch ( $custom_query );

			$this->result_query = $custom_query;

			$this->result = $query_result;
			
		}

	}

	public function get_result_query() {

		return $this->result_query;

	}
	
	public function get_result() {

		return $this->result;

	}
	
	public function get_result_single() {

		return $this->result[0];

	}
	
	public function get_result_count() {
		
		$result_count = 0;
		
		if ( ! empty( $this->result ) ) {
			
			$result_count = $this->fetch_count( $this->_document_type, $this->_search_array );
			
		}
		
		return $result_count;
		
	}
	
}
