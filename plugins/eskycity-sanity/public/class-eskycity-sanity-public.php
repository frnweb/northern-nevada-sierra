<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/public
 * @author     eSkyCity <support@eskycity.com>
 */
class Eskycity_Sanity_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eskycity_Sanity_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eskycity_Sanity_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/eskycity-sanity-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eskycity_Sanity_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eskycity_Sanity_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/eskycity-sanity-public.js', array( 'jquery' ), $this->version, false );
		
		$output = new Eskycity_Sanity_Output();
		
		if ( $output->use_autocomplete() ) {
			
			wp_enqueue_script($this->plugin_name . '-search-autocomplete', plugin_dir_url(__FILE__) . 'js/eskycity-sanity-search-autocomplete.js', array(), $this->version, false);
			
		}
		
	}
	
	function register_query_vars( $vars ) {
		
		$url_settings = get_option( 'eskycity_sanity_urls' );
		
		// Check if URL settings exist
		if ( is_array( $url_settings ) && ! empty( $url_settings ) ) {
		
			// Loop through settings
			foreach ( $url_settings as $key => $value ) {
				
				$key = strtolower( esc_attr ( $key ) );
				$value = strtolower( esc_attr ( $value ) );
				
				// queryvar found with value
				if ( ( substr( $key, 0, 9 ) == 'queryvar_' ) && ! empty( $value ) ) {
					
					// Add query variable to array for WordPress
					$vars[] = $value;
					
				}
			}

		}
		
		return $vars;
	}
}
