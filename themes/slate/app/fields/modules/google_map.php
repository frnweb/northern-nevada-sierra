<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 50],
];


$map = new FieldsBuilder('map');

$map
    ->addGroup('map')

    //Logo checkbox
    ->addTrueFalse('add_markers', [
    'wrapper' => ['width' => 15]
    ])

    //Repeater
	->addRepeater('marker', [
        'min' => 1,
        'max' => 7,
        'button_label' => 'Add Marker',
        'layout' => 'block',
        'wrapper' => ['width' => 85]
        
    ])
    ->conditional('add_markers', '==', 1)

    //Google Map
    ->addGoogleMap('google_map')
    
    ->endRepeater()
  
    ->endGroup();

return $map;