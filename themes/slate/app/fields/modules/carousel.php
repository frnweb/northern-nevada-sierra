<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],
];

$carousel = new FieldsBuilder('carousel');

$carousel
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$carousel
	->addTab('carousel_type', ['placement' => 'left'])
		->addSelect('type_select', [
			'label' => 'Carousel Type',
			'wrapper' => ['width' => 20]
		])
	  	->addChoices(
	  	['card' => 'Card'],
	  	['duo' => 'Duo']
		)

	->addTab('content', ['placement' => 'left'])

		->addGroup('slides_to_show', [
			'conditional_logic' => [
				[
					[
					'field' => 'type_select',
					'operator' => '==',
					'value' => 'card',
					],
				],
			],
		])

			// Large
			->addSelect('slides_large', [
				'label' => 'Large Device',
				'wrapper' => ['width' => 50]
			])
		  	->addChoices(
		  	['1' => '1'],
		  	['2' => '2'],
		  	['3' => '3'],
		  	['4' => '4']
		  	)
		  	// Medium
			->addSelect('slides_medium', [
				'label' => 'Medium Device',
				'wrapper' => ['width' => 50]
			])
		  	->addChoices(
		  	['1' => '1'],
		  	['2' => '2'],
		  	['3' => '3'],
		  	['4' => '4']
			)
			->endGroup()

    	//Repeater
		->addRepeater('carousel', [
		  'min' => 1,
		  'max' => 10,
		  'button_label' => 'Add Item',
		  'layout' => 'block',
		  'wrapper' => [
	          'class' => 'deck',
	        ],
		])

		//Image 
		->addImage('carousel_image')

		->addFields(get_field_partial('modules.card'));
	  	

return $carousel;