<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$post_type_search = new FieldsBuilder('post_type_search');

$post_type_search
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'))

		// Set Post Type for Search
		->addText('post_type', [
			'label' => 'Post Type',
			'ui' => $config->ui
		])
		->setInstructions('Add name of Post Type (ex. sl_post_cpts)');

$post_type_search
    ->addTab('content', ['placement' => 'left'])

    	// Header
		->addText('header', [
			'label' => 'Header',
			'ui' => $config->ui
		])

    	//Add Module Button
		->addTrueFalse('module_button', [
			'wrapper' => ['width' => 30]
			])
			->setInstructions('Check to add a button to the bottom of the module')
		->addLink('button', [
			'wrapper' => ['width' => 70]
		])
		->conditional('module_button', '==', 1)

		//Add Featured posts
		->addTrueFalse('add_featured', [
			'label' => 'Add Featured Posts',
		])
		// Header
		->addText('title', [
			'label' => 'Title',
			'ui' => $config->ui
		])
	    // Post Relationship Field
		->addRelationship('featured', [
		    'label' => 'Post Type Picker',
	        'min' => 1,
			'max' => 12,
		    'ui' => $config->ui,
	    ])
	    ->conditional('add_featured', '==', 1);


return $post_type_search;