<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 25],
	'half' 		=> ['width' => 50],
];

$button = new FieldsBuilder('button');

$button

	//Button Group checkbox
	->addTrueFalse('button_group', [
		'wrapper' => ['width' => 15]
	])
		->setInstructions('Sets the buttons as a group with wrapper')

	// Start Flexible Content for Buttons
	->addFlexibleContent('buttons', [
		'button_label' => 'Add Button Type',
		'wrapper' => ['width' => 85]
	])
	
		// Default Button
			->addLayout('Default Button')
				->addTrueFalse('class_field', [
					'label' => 'Class',
					'wrapper' => ['width' => 10]
				])
				->addText('button_class', [
					'wrapper' => ['width' => 20]
				])
				->conditional('class_field', '==', 1 )
				->addLink('button', [
					'wrapper' => ['width' => 70]
				])
				
		// Modal Button
			->addLayout('Modal Button')
			 	->addTrueFalse('class_field', [
					'label' => 'Class',
					'wrapper' => ['width' => 10]
				])
				->addText('button_class', [
					'wrapper' => ['width' => 20]
				])
				->conditional('class_field', '==', 1 )
			 	->addText('title', [
			 		'wrapper' => ['width' => 60]
			 	])
			 	->addText('modal_target', [
					'label' => 'Modal Target',
					'ui' => $config->ui
			 	])
				->setInstructions('Data ID associated with modal popup')

	 	// FRN Phone Button
		->addLayout('Phone Button')
			 
			->addTrueFalse('cta_field', [
					'label' => 'CTA Field',
					'wrapper' => ['width' => 10]
				])
				->addText('cta_text', [
					'label' => 'CTA Text',
					'wrapper' => ['width' => 40]
				])
				->conditional('cta_field', '==', 1 )

			->addTrueFalse('class_field', [
					'label' => 'Class',
					'wrapper' => ['width' => 10]
				])
				->addText('button_class', [
					'wrapper' => ['width' => 40]
				])
				->conditional('class_field', '==', 1 )
				
	 	// Share Button
		->addLayout('Share Button')
			 
		->addTrueFalse('class_field', [
				'label' => 'Class',
				'wrapper' => ['width' => 10]
			])
			->addText('button_class', [
				'wrapper' => ['width' => 40]
			])
			->conditional('class_field', '==', 1 )

		// Chat Button
		->addLayout('Chat Button')
			 
		->addTrueFalse('class_field', [
				'label' => 'Class',
				'wrapper' => ['width' => 10]
			])
			->addText('button_class', [
				'wrapper' => ['width' => 40]
			])
			->conditional('class_field', '==', 1 );	

return $button;