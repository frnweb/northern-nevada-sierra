<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$map = new FieldsBuilder('map');

$map
	//Settings
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'))
		->addTrueFalse('map_only', [
			])
			->setInstructions('Check to hide the card view and only display the map');
			
$map
	->addTab('content', ['placement' => 'left'])

		//Header
		->addText('header', [
			'label' => 'Header'
	    	])
	    	->setInstructions('This is optional')

	    ->addTrueFalse('module_button', [
			'wrapper' => ['width' => 30]
			])
			->setInstructions('Check to add a button to the bottom of the module')
		->addLink('button', [
			'wrapper' => ['width' => 70]
		])
		->conditional('module_button', '==', 1);

return $map;