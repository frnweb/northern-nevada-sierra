import 'babel-polyfill'
import whatInput from 'what-input'

import googleMaps from './lib/google-maps'
import gaEvents from './lib/ga-events'
import slickCarousel from './lib/slick-carousel'
import hamburger from './lib/hamburger'
import contactMobile from './lib/contact-mobile'
import notificationbarMobile from './lib/notificationbar-mobile'
import backTop from './lib/back-to-top'
import responsiveEmbed from './lib/responsive-embed'
import mapModule from './lib/map-module'
import postTypeSearch from './lib/post-type-search'

jQuery(function($) {
    jQuery(document).ready(function() {
        $(document).foundation();
    });
});