import $ from 'jquery'
  
  //GA EVENTS
jQuery(function($) {
  jQuery(document).ready(function() {
  
    var label = window.location.pathname;
  
    //GA Event for Slick Carousel prev button
    $('.slick-prev').click(function() {
      ga('send', 'event', 'carousel', 'previous', 'carousel on' + label);
    });
  
    //GA Event for Slick Carousel next button
    $('.slick-next').click(function() {
      ga('send', 'event', 'carousel', 'next', 'carousel on' + label);
    });
  
    //GA Event for open Modal button
    $('.modal-button').click(function() {
      ga('send', 'event', 'modal', 'open', 'modal open on' + label);
    });
  
    //GA Event for close Modal button
    $('.close-button').click(function() {
    ga('send', 'event', 'modal', 'close', 'modal close on' + label);
    });
  
    //GA Event for Menu search button
    $('.sl_button--search').click(function() {
      ga('send', 'event', 'menu search', 'click', 'Menu Search on ' + label);
    });
  
     //GA Event for Mobile Menu open button
    $('.is-active, .hamburger--collapse').click(function() {
      ga('send', 'event', 'mobile menu toggled', 'click', 'Mobile menu toggled on' + label);
    });
  });
});