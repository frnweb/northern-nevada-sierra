jQuery(function($) {
  function initSlider(){
    //TESTIMONIAL MODULE
    $(".sl_testimonial__slider").slick({

      // normal options...
      prevArrow: `<i class="sl_testimonial__prev sl_testimonial__arrows fas fa-chevron-left"></i>`,
      nextArrow: `<i class="sl_testimonial__next sl_testimonial__arrows fas fa-chevron-right"></i>`,
      infinite: true,
      dots: true,
      slidesToShow: 1,
      appendArrows: '.sl_testimonial__controls',
      appendDots: '.sl_testimonial__controls',
      adaptiveHeight: true,
    });

    //SERVICESLOCATION SLIDER
    $(".sl_locations__slider").slick({

      // normal options...
      prevArrow: `<i class="sl_locations__prev sl_locations__arrows fas fa-chevron-left"></i>`,
      nextArrow: `<i class="sl_locations__next sl_locations__arrows fas fa-chevron-right"></i>`,
      infinite: true,
      slidesToShow: 3,
      appendArrows: '.sl_locations__slider-controls',

      // the magic
          responsive: [{
        
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
              }
        
            },
            {
        
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
              }
        
            }]
    });

    //CAROUSEL MODULE
    const carouselSlider = [];

    const carouselDuoSlider = [];

    const CarouselImageSlider = [];

    var carouselExists = document.getElementsByClassName('sl_carousel');
    if (carouselExists.length > 0) {
    // if carousel module exists on page

    myCarouselData.forEach(function(item){
      if (item.type == 'card') {
        carouselSlider.push(item);
      } else if (item.type == 'duo'){
        carouselDuoSlider.push(item);
      } else {
        carouselImageSlider.push(item);
      }
    })

    //Carousel Card Type

      carouselSlider.forEach(function(item){
        $( '.sl_slider--' + item.loopIndex ).slick({

          // normal options...
          prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
          nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
          infinite: true,
          slidesToShow: item.slidesLarge,
        
          // the magic
          responsive: [{
        
              breakpoint: 1024,
              settings: {
                slidesToShow: item.slidesMedium,
                infinite: true
              }
        
            }, {
        
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                infinite: true
              }
        
            }]
      });
      })


      //Carousel Duo Type
      carouselDuoSlider.forEach(function(item){
        $( '.sl_slider-duo--' + item.loopIndex ).slick({

            // normal options...
            prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
            nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
            infinite: true,
            slidesToShow: 1,
            arrows: false,
            dots: true
        });
      });
    };
  };
  jQuery(document).ready(function() {
    initSlider();
  });
});